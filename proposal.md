
## 1. Name and Contact Information

* Name - Ganesh Tiwari
* Email - ganesht049@gmail.com
* Website - ganesh.social
* IRC - gat786
* Address - D 107, Govinda Park, Nalasopara West, Mumbai
* Phone - 8796079680

# 2. Title

## GNOME.org: Evaluation of requirements, content, design and components for the refresh of the website.

# 3. Synopsis

This project directly deals with the idea of redesigning of the website gnome.org taking into consideration of the contents of the site and making the website looking modern and increasing the UI and UX of the currently available system. 

Majorly looking the website has a simple theme and format which is used throughout the website. This thing is going to be done using sass and using other modern web technologies(nodejs is going to be our choice). Rather than using a design scheme of others GNOME could benefit a lot if it has a design scheme of its inspired from GTK and its based application having the look and feel of Original GNOME environment.


# 4. Benefits to the Community

Benefits of this project will be enormous and inspire a better designing approach across all of GNOME. Having specifications for every elements and following them will result in a much better User Experience while retaining the usefullness and being much more beautiful. 

Moreover after this change the whole of GNOME will look more unified and modern. Interactivity with users is a great way to reach users hearts and that will be a major benefits that I plan on giving on to GNOME community

# 5. Implementation Plan


This project will consist of these principal components:-

1. Standard UI Elements Designing-
   
   Inorder to recreate a new design for gnome it will be necessary that we pick necessary elements for our design overhaul. We will create elements that can be reused throughout the web portal and make it standardize so that no part of the website feels out of order.

2. Redesigning themes for GNOME.org web platform
   
   We will create themes that would involve one light theme and a dark theme that users can switch according to their will whenever they want making it a pleasing and fully controllable viewing experience for every visitor.

3. Automating the task of generating static sites using Jekyll.
   
   Writing websites for non technical and people who dont write much code can be tedious where comes static site generators like jekyll which enable us to create static sites from just plain texts. It can come in handy when people want to create a blog quickly without dealing with the hassles of writing actual html.


# 6. Deliverables

* A new redesigned elements for GNOME web portal.
* Modern UI features which enable users to toggle themes and have a better experience while browsing GNOME web interface.
* A new way to write blogs which enables people with no experience in writing code to write blogs without using any other software other than the GNOME Web interface itself.

# 7. Related Work

While I would not declare myself as a expert Web developer i have given my fair part to the community i came from and hope to provide the same to the overall GNOME community. Here are some web development projects that i have completed and are on Internet currently live on the internet.

1. College Website for the College i currrently study in
   
   https://rdnational.ac.in
2. A website that automatically generates and certificates to attendees of Microsoft Student Partner events when provided with correct data.
   
   https://mspcert.azurewebsites.net

3. My current not so awesome Personal Portfolio which i had built a long before this project.
   https://www.ganesh.social



# 8. Proposal Timeline

Before April 20:

Familiarize myself with the structure of content of GNOME.org website and how the files are stored and its architecture.

Study how to write SASS more efficiently.

Planning a structure that we would use for building our new restructured website.

Learning more about static sites generator Jekyll and implementing them to know how they work.p


April 20 - April 27

Trying to practice everything i have learnt and tring to sharp my skills.

April 27 - May 15

Assesing the current GNOME web portal and continously working on ideas on how to make it better.

May 16 - June 25

Creating the UI Kit according the assessment made in previous stage while making sure that the requirements are met throughout this process. During this period I also plan on integrating these assets built with the jekyll engine so that it can have a basic usability and we can also start working on its usage

June 25 - July 5 

Reviewing all the designs and testing out each element through unit, integration and other tests to make sure the product built is bugs free.

Final 10 days are kept as a buffer so that we can cope up with the difficulties if we face any during the prvious two phases.
