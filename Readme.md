# Idea

GNOME.org: Evaluation of requirements, content, design and components for the refresh of the website. (mentor: ClaudioSantoro, co-mentors: Engagement Team, co-mentors: NeilMcGovern, Caroline Henriksen)

Brief explanation: The GNOME.org website needs a redesign (refresh) both in design terms and look-and-feel (UI) terms. This project is about the technical and content-wise evaluation for a possible refresh of the GNOME.org website. This project would include the assessment of the current state of GNOME.org by its content and then a full evaluation of design-wise and content-wise changes.

Goals: Our goals within this GSoC project are the assessment of current GNOME.org followed by an evaluation of a future GNOME.org website. Including also a development of a UI library (Framework) made of UI components (Colours, Grid, Buttons, Containers, etc) that would be used as building bricks (Foundation) for the next GNOME.org website.

Requirements: Good communication skills, observational skills and writing skills are a plus. Experience with SCSS/Sass, ES6 (JavaScript/EcmaScript) is also welcome. Having experience with Node.js, Static-Page-Generators like Jekyll and other web development tools are also a big plus.

Communication: The announcements will be made on Discourse. Our internal communication will be done through our Rocket.Chat (https://chat.gnome.org) on the channel #outreach. GitLab will be used for planning, issues discussion and development. We may also use additional video-conference tools and/or communicate also through e-mails.

# Sample Headlines in a proposal

1. Abstract
2. Implementation Plan
3. Deliverables 
4. Timeline

# GSOC Recommendations

Name and Contact Information

Putting your full name on the proposal is not enough. Provide full contact information, including email addresses, websites, IRC nick, postal address and telephone number.

Title

Your title should be short, clear and interesting. The job of the title is to convince the reviewer to read your synopsis.

Synopsis

If the format allows, start your proposal with a short summary, designed to convince the reviewer to read the rest of the proposal.

Benefits to Community

Don’t forget to make your case for a benefit to the organization, not just to yourself. Why would Google and your organization be proud to sponsor this work? How would open source or society as a whole benefit? What cool things would be demonstrated?

Deliverables

Include a brief, clear work breakdown structure with milestones and deadlines. Make sure to label deliverables as optional or required. You may want your plan to start by producing some kind of white paper, or planning the project in traditional Software Engineering style. It’s OK to include thinking time (“investigation”) in your work schedule. Deliverables should include investigation, coding and documentation.

Related Work

You should understand and communicate other people’s work that may be related to your own. Do your research, and make sure you understand how the project you are proposing fits into the target organization. Be sure to explain how the proposed work is different from similar related work.

Biographical Information

Keep your personal info brief. Be sure to communicate personal experiences and skills that might be relevant to the project. Summarize your education, work, and open source experience. List your skills and give evidence of your qualifications. Convince your organization that you can do the work. Any published work, successful open source projects and the like should definitely be mentioned.

Follow the Rules

Most organizations accept only plain text applications. Most organizations have a required application format. Many organizations have application length limits. In general, organizations will throw out your proposal if you fail to conform to these guidelines. If you feel you must have graphical or interactive content associated with your application, put just this content (not a copy of your proposal) on the web and provide an easy-to-type URL. Do not expect reviewers to follow this link.
